FROM ubuntu:14.04

ENV USER mpirun

ENV DEBIAN_FRONTEND=noninteractive \
    HOME=/home/${USER} 

ENV MPI_HOSTS_FILE=$HOME/mpi_hosts

RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    apt-utils \
    gfortran \
    git \
    wget \
    make \
    vim \
    wget \
    file \
    openssh-server \
    openssh-client \
    libblas-dev \
    liblapack3 \
    libboost-dev \
    liblapack-dev \
    libopenblas-base \
    libopenblas-dev \
    libatlas-base-dev \
    liblapacke-dev \
    libopenmpi-dev \
    libscalapack* \
    openmpi-bin \
    openmpi-common \
    curl \
    nfs-kernel-server nfs-common \
    ca-certificates \
    && apt-get clean \
    && apt-get purge \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir /var/run/sshd
RUN echo 'root:${USER}' | chpasswd
RUN sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

# ------------------------------------------------------------
# Add an 'mpirun' user
# ------------------------------------------------------------

RUN adduser --disabled-password --gecos "" ${USER} && \
    echo "${USER} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

# ------------------------------------------------------------
# Set-Up SSH with our Github deploy key
# ------------------------------------------------------------

ENV SSHDIR ${HOME}/.ssh/

RUN mkdir -p ${SSHDIR}

COPY files/config ${SSHDIR}/config
COPY files/id_rsa.mpi ${SSHDIR}/id_rsa
COPY files/id_rsa.mpi.pub ${SSHDIR}/id_rsa.pub
COPY files/id_rsa.mpi.pub ${SSHDIR}/authorized_keys

RUN chmod -R 600 ${SSHDIR}* && \
chown -R ${USER}:${USER} ${SSHDIR}

# Or we can compile OpenMPI manually
# WORKDIR /home/
# RUN wget --no-check-certificate http://www.open-mpi.org/software/ompi/v2.1/downloads/openmpi-2.1.1.tar.gz

# RUN tar xzvf openmpi-2.1.1.tar.gz

# WORKDIR openmpi-2.1.1

# RUN ./configure
# RUN make -j4 BINARY=64
# RUN make install
# RUN ldconfig

# RUN which mpicc
# RUN mpicc -show
# RUN which mpiexec
# RUN mpiexec --version

# install OpenBlas
RUN apt-get install -y git
RUN git clone https://github.com/xianyi/OpenBlas.git
WORKDIR OpenBlas/
RUN make clean
RUN make -j4 BINARY=64
RUN mkdir /usr/OpenBLAS
RUN chmod o+w,g+w /usr/OpenBLAS/
RUN make PREFIX=/usr/OpenBLAS install
RUN ldconfig
RUN ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/libblas.so
RUN ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/libblas.so.3
RUN ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/libblas.so.3.5
RUN ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/liblapack.so
RUN ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/liblapack.so.3
RUN ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/liblapack.so.3.5

# ------------------------------------------------------------
# Configure OpenMPI
# ------------------------------------------------------------

RUN rm -fr ${HOME}/.openmpi && mkdir -p ${HOME}/.openmpi && mkdir $HOME/cloud
COPY files/default-mca-params.conf ${HOME}/.openmpi/mca-params.conf
RUN chown -R ${USER}:${USER} ${HOME}/.openmpi

WORKDIR /home/
# RUN mkdir OpenMpi
# WORKDIR /home/OpenMPi

# RUN wget --no-check-certificate http://www.open-mpi.org/papers/workshop-2006/hello.c
# RUN mpicc hello.c -o hello
# RUN mpirun -np 4 ./hello

# RUN curl -L http://www.netlib.org/scalapack/scalapack_installer.tgz | tar -zx

COPY files/* $HOME/

RUN echo "export MPI_HOSTS_FILE=$HOME/mpi_hosts >> /etc/profile" \
    && echo "export NUM_SLOTS=$NUM_SLOTS >> /etc/profile" \
    && echo "export MPI_USER=$USER >> /etc/profile" \
    && . /etc/profile && chown -R ${USER}:${USER} $HOME/* \
    && touch ${MPI_HOSTS_FILE} && chown ${USER}:${USER} ${MPI_HOSTS_FILE}


# WORKDIR $HOME/cloud

EXPOSE 22

ENTRYPOINT ["/home/mpirun/bootstrap.sh"]