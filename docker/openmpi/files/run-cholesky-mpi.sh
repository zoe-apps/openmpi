#!/bin/bash

function display_help(){
    cat <<EOF
    USAGE: "Usage $0 -n [OPTIONS] matrix_size num_processes_per_row"

    OPTIONS

        --matrix_size       The size of input matrix
        --pprow             The number of processes per row (We use a grid of processes)
        --block_size        Size of each block
        --hostfile          The host file of mpi cluster
        --mca_opts          The mca options for MPI (if any). Otherwise, the default options is applied.

EOF
}

if [[ $# == 0 ]]; then
    display_help
    exit 1
fi

MATRIX_SIZE=40000
BLOCK_SIZE=1000
PPROW=1
MPI_HOSTS_FILE=/home/mpirun/mpi_hosts
MCA_OPTIONS=""

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --mca_opts)
    MCA_OPTIONS="$2"
    shift # past argument
    shift # past value
    ;;
    --pprow)
    PPROW="$2"
    shift # past argument
    shift # past value
    ;;
    --matrix_size)
    MATRIX_SIZE="$2"
    shift # past argument
    shift # past value
    ;;
    --hostfile)
    [ -n "$2" ] && MPI_HOSTS_FILE="$2"
    echo "Hostfile: $MPI_HOSTS_FILE"
    shift # past argument
    shift # past value
    ;;
    --block_size)
    BLOCK_SIZE="$2"
    shift # past argument
    shift # past value
    ;;
    -h|--help)
    display_help
    return;
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

MCA_OPTIONS=$(echo $MCA_OPTIONS | sed 's/:/ /g')

echo "Running algo with $PPROW*$PPROW processes"
echo "MPI mca options from user: $MCA_OPTIONS"
# echo "Full options: $(ompi_info --all)"

MPI_COMMAND="mpirun $([ -n "$MCA_OPTIONS" ] && echo $MCA_OPTIONS) -v -np $(( $PPROW*$PPROW )) --hostfile $MPI_HOSTS_FILE $SHARED_DIR/cpp_mpi_scalapack  $MATRIX_SIZE $BLOCK_SIZE $PPROW"
echo "Run command: $MPI_COMMAND"

eval $MPI_COMMAND

cd $DIR



