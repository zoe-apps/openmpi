#!/bin/bash

export MPI_USER=mpirun

export DEBIAN_FRONTEND=noninteractive
export MPI_HOME=/home/${MPI_USER} 

export MPI_HOSTS_FILE=${MPI_HOME}/mpi_hosts
export NUM_SLOTS=1

sudo apt-get update && sudo apt-get install -y --no-install-recommends \
    build-essential \
    apt-utils \
    gfortran \
    git \
    wget \
    make \
    vim \
    wget \
    file \
    openssh-server \
    openssh-client \
    libblas-dev \
    liblapack3 \
    libboost-dev \
    liblapack-dev \
    libopenblas-base \
    libopenblas-dev \
    libatlas-base-dev \
    liblapacke-dev \
    libopenmpi-dev \
    openmpi-bin \
    openmpi-common \
    curl \
    nfs-kernel-server nfs-common \
    && sudo apt-get clean \
    && sudo apt-get purge \
    && sudo rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

sudo mkdir /var/run/sshd
sudo echo 'root:${MPI_USER}' | chpasswd
sudo sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
sudo sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

export NOTVISIBLE="in users profile"
sudo echo "export VISIBLE=now" >> /etc/profile

# ------------------------------------------------------------
# Add an 'mpirun' user
# ------------------------------------------------------------

sudo adduser --disabled-password --gecos "" ${MPI_USER} && \
    sudo echo "${MPI_USER} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

# ------------------------------------------------------------
# Set-Up SSH with our Github deploy key
# ------------------------------------------------------------

export SSHDIR=${MPI_HOME}/.ssh/

mkdir -p ${SSHDIR}

cp ./config ${SSHDIR}/config
cp ./id_rsa.mpi ${SSHDIR}/id_rsa
cp ./id_rsa.mpi.pub ${SSHDIR}/id_rsa.pub
cp ./id_rsa.mpi.pub ${SSHDIR}/authorized_keys

chmod -R 600 ${SSHDIR}* && \
chown -R ${MPI_USER}:${MPI_USER} ${SSHDIR}

# ------------------------------------------------------------
# Configure OpenMPI
# ------------------------------------------------------------

rm -fr ${MPI_HOME}/.openmpi && mkdir -p ${MPI_HOME}/.openmpi
cp ./default-mca-params.conf ${MPI_HOME}/.openmpi/mca-params.conf
chown -R ${MPI_USER}:${MPI_USER} ${MPI_HOME}/.openmpi

cp ./* ${MPI_HOME}/