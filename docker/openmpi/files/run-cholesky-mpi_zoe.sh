#!/bin/bash

# This file is used to run experiment with ZOE only !!!!!


function display_help(){
    cat <<EOF
    USAGE: "Usage $0 -n [OPTIONS]"

    NOTE: This file is used to run experiment with ZOE only !!!!!

    OPTIONS

        --matrix_size       The size of input matrix
        --pprow             The number of processes per row (We use a grid of processes)
        --block_size        Size of each block
        --hostfile          THe host file of mpi cluster
        --num_workers       Number of worker
        --master_memory     The amount memory of the OpenMPI master (in GB)
        --worker_memory     The amount memory of each OpenMPI workers (in GB)
        --worker_cores      The number of cores in each worker
        --master_cores      The number of cores of the machine that submits the application

EOF
}

if [[ $# == 0 ]]; then
    display_help
    exit 1
fi

MATRIX_SIZE=40000
BLOCK_SIZE=1000
PPROW=1
MPI_HOSTS_FILE=/home/mpirun/mpi_hosts
NUM_WORKERS=10
MASTER_MEMORY=12 # ~12GBs
WORKER_MEMORY=32 # 32 GBs
WORKER_CORES=6
MASTER_CORES=$WORKER_CORES
MCA_OPTIONS=""

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --mca_opts)
    MCA_OPTIONS="$2"
    shift # past argument
    shift # past value
    ;;
    --num_workers)
    NUM_WORKERS="$2"
    shift # past argument
    shift # past value
    ;;
    --worker_memory)
    WORKER_MEMORY="$2"
    shift # past argument
    shift # past value
    ;;
    --worker_cores)
    WORKER_CORES="$2"
    shift # past argument
    shift # past value
    ;;
    --master_memory)
    MASTER_MEMORY="$2"
    shift # past argument
    shift # past value
    ;;
    --master_cores)
    MASTER_CORES="$2"
    shift # past argument
    shift # past value
    ;;
    --pprow)
    PPROW="$2"
    shift # past argument
    shift # past value
    ;;
    --matrix_size)
    MATRIX_SIZE="$2"
    shift # past argument
    shift # past value
    ;;
    --hostfile)
    MPI_HOSTS_FILE="$2"
    shift # past argument
    shift # past value
    ;;
    --block_size)
    BLOCK_SIZE="$2"
    shift # past argument
    shift # past value
    ;;
    -h|--help)
    display_help
    return;
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Running algo with $PPROW*$PPROW processes"

properties=${PPROW}-per-row-${BLOCK_SIZE}
filename=scalapack-cholesky-${MATRIX_SIZE}-$properties.json

WORKER_MEMORY=$(awk "BEGIN { print $WORKER_MEMORY*1024*1024*1024 }"  )
MASTER_MEMORY=$(awk "BEGIN { print $MASTER_MEMORY*1024*1024*1024 }"  )

[ -f "/mnt/workspace/scripts/custom_script_pre.sh" ] && bash /mnt/workspace/scripts/custom_script_pre.sh

sed -e "s/\${PPROW}/$PPROW/g" \
    -e "s/\${BLOCK_SIZE}/$BLOCK_SIZE/g" \
    -e "s/\${MATRIX_SIZE}/$MATRIX_SIZE/g" \
    -e "s/\${NUM_WORKERS}/$NUM_WORKERS/g" \
    -e "s/\${MASTER_MEMORY}/$MASTER_MEMORY/g" \
    -e "s/\${WORKER_MEMORY}/$WORKER_MEMORY/g" \
    -e "s/\${MASTER_CORES}/$MASTER_CORES/g" \
    -e "s/\${WORKER_CORES}/$WORKER_CORES/g" \
    -e "s/\${MCA_OPTIONS}/$MCA_OPTIONS/g" \
    $DIR/zapp_cholesky_scalapack.json > /tmp/$filename

workon zoe_executor; cd ~/research/gaussian-processes/

# cat /tmp/$filename;

cd ~/projects/zoe; 

id=$(~/projects/zoe/zoe.py  start "scalapack cholesky $MATRIX_SIZE $properties" /tmp/$filename | egrep -o "[0-9]+")
status="unknown"
last_status=""
if [ "$id" != "" ]; then
    while [ "$status" != "terminated" ] && [ "$status" != "error" ] ; do
        sleep 1s
        status=$(~/projects/zoe/zoe.py exec-get $id | grep Status | sed -e "s/Status:\s\(.*\)/\1/")
        if [ "$status" != "$last_status" ]; then
            last_status=$status
            echo "Status: $status"
        fi
    done
    echo "Finish !"
fi
cd -;

cd $DIR



