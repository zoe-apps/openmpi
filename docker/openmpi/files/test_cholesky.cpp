// This program is inspired from https://andyspiros.wordpress.com/2011/07/08/an-example-of-blacs-with-c/

#include <mpi.h>
#include <stdlib.h>
 
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <sstream>
#include <time.h>
#include <chrono>
#include <math.h>
 
using namespace std;
 
extern "C" {
    /* Cblacs declarations */
    void Cblacs_pinfo(int*, int*);
    void Cblacs_get(int, int, int*);
    void Cblacs_gridinit(int*, const char*, int, int);
    void Cblacs_gridinfo(int, int*, int*, int*,int*);
    void Cblacs_pcoord(int, int, int*, int*);
    void Cblacs_gridexit(int);
    void Cblacs_barrier(int, const char*);
    void Cdgerv2d(int, int, int, double*, int, int, int);
    void Cdgesd2d(int, int, int, double*, int, int, int);

    int numroc_(int*, int*, int*, int*, int*);

    void pdpotrf_(char*, int*, double*,
            int*, int*, int*, int*);

    void descinit_( int *, int *, int *, int *, int *, int *, int *,
          int *, int *, int *);
}
 

template<typename TimeT = std::chrono::microseconds, 
    typename ClockT=std::chrono::high_resolution_clock,
    typename DurationT=double>
class Stopwatch
{
private:
    std::chrono::time_point<ClockT> _start, _end;
public:
    Stopwatch() { start(); }
    void start() { _start = _end = ClockT::now(); }
    DurationT stop() { _end = ClockT::now(); return elapsed();}
    DurationT elapsed() { 
        auto delta = std::chrono::duration_cast<TimeT>(_end-_start);
        return delta.count(); 
    }
};

double* allocate_memory(int N) 
{
    double* arr;
    const size_t alignment = sizeof(size_t);  // change the 16 to 64 if you wish to align to the cache line
    int status = posix_memalign((void **)&arr, ( (alignment >= sizeof(void*)) ? alignment : sizeof(void*) ), sizeof(double)*N*N);
    arr = (status == 0) ? arr : NULL;
    return arr;
}

int main(int argc, char **argv)
{
    int mpirank, nprocs_mpi;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpirank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs_mpi); 
    bool mpiroot = (mpirank == 0);
    const int root=0;
    double MPIelapsed;
    double MPIt2;
    double MPIt1;

    const int MAX_MSIZE_SINGLE = 40000; // the maximum size of matrix that can be allocated in a single machine

    /**
     * In this test, we try to factorize a definite symmetric matrix into L*L^T using Cholesky decomposition algorithm.
     * We use function pdpotrf_ of LAPACK/ScaLAPACK
     * With large matrices, e.g. N >= 50000, we cannot allocate memory for matrix NxN, 
     * then we use a similation to generate matrix blocks on each processor
     * rather than distributing the whole huge matrix from the master.
     */ 
 
    /* Helping vars */
    int iZERO = 0;
 
    if (argc < 4) {
        if (mpiroot)
            cerr << "Usage: " << argv[0] << "N Nb np_rows" << endl;
        MPI_Finalize();
        return 1;
    }
 
    int N, Nb, Nprows=1;
    double *A_glob = NULL, *A_glob2 = NULL, *A_loc = NULL;
    int ord = 8;
    int verbose = 1;

    /**
     * 
     * PARSE COMMANDLINE ARGS
     * 
     */
    /* Parse command line arguments */
    if (mpiroot) {
        /* Read command line arguments */
        // stringstream stream;
        // stream << argv[1] << " " << argv[2] << " " << argv[3];
        // stream >> N >> Nb >> Nprows;
        N = atoi(argv[1]);
        Nb = atoi(argv[2]);
        Nprows = atoi(argv[3]);
        Nb = ceil(N*1.0/Nprows);
        ord = N;

        cout << "Matrix size: " << N << " Block size: " << Nb << " Num processes per row: " << Nprows << endl;
    }


    /**
     * 
     * INIT CONTEXT AND BROADCAST VARIABLES
     * 
     */ 

    char hostname[256];
    int name_len;
    MPI_Get_processor_name(hostname, &name_len);
    cout << "Hello world!  I am process number: " << mpirank << " on host" << hostname << endl;
    cout << "Begin initializing context on process " << mpirank << " on host" << hostname << "..." << endl;

    int ctxt, myid, myrow, mycol, numproc;
    int procrows = Nprows, proccols = Nprows;

    /* Broadcast of the matrix dimensions */
    int dimensions[3];
    if (mpiroot) {
        dimensions[0] = N;   // Global Rows
        dimensions[1] = Nb;  // Block size
        dimensions[2] = Nprows; // Num processes per row
        cout << "I want to send nprows=" << Nprows << endl;
        
    }

    MPI_Bcast(dimensions, 3, MPI_INT, root, MPI_COMM_WORLD);
    MPI_Bcast(&ord, 1, MPI_INT, root, MPI_COMM_WORLD);

    N = dimensions[0];
    Nb = dimensions[1];
    Nprows = dimensions[2];
    procrows = proccols = Nprows;

    Cblacs_pinfo(&myid, &numproc);
    Cblacs_get(0, 0, &ctxt);
    Cblacs_gridinit(&ctxt, "Row-major", procrows, proccols);
    Cblacs_gridinfo( ctxt, &procrows, &proccols, &myrow, &mycol );
    /* process coordinates for the process grid */
    Cblacs_pcoord(ctxt, myid, &myrow, &mycol);

    if (mpiroot){
        cout << "Broadcasting variables..." << endl;
    }

    int nrows = numroc_(&N, &Nb, &myrow, &iZERO, &procrows);
    int ncols = numroc_(&N, &Nb, &mycol, &iZERO, &proccols);

    int lda = max(1,nrows);

    MPI_Bcast(&lda, 1, MPI_INT, 0, MPI_COMM_WORLD);
 
    /* Print grid pattern */
    if (myid == 0)
        cout << "Processes grid pattern:" << endl;
    for (int r = 0; r < procrows; ++r) {
        for (int c = 0; c < proccols; ++c) {
            Cblacs_barrier(ctxt, "All");
            if (myrow == r && mycol == c) {
                cout << myid << " " << flush;
            }
        }
        Cblacs_barrier(ctxt, "All");
        if (myid == 0)
            cout << endl;
    }
    



    clock_t tStart = clock();

    bool can_alloc = false;
 
    /* Allocate memory */
    if (mpiroot) {
        
        // A_glob = allocate_memory(N);

        if(A_glob == NULL)
        {
            printf("Memory allocation failed");
            // return 1;
        } else {
            can_alloc = true;
        }
        // A_glob2 = new double[N*N];

        if (can_alloc){
            srand(time(0));

            for (int i = 0; i < N * N; ++i)
            {
                A_glob[i] = (double)rand() / RAND_MAX;
            }

            double tmp = 0;
            for (int i = 0; i < N; ++i)
            {
                for (int j = i; j < N; ++j)
                {
                    tmp = (A_glob[i * N + j] + A_glob[j * N + i]) / 2;
                    if (i == j)
                    {
                        tmp += N;
                    }
                    A_glob[i * N + j] = A_glob[j * N + i] = tmp;
                }
            }

            printf("{%% generating_matrix %%}{%% %.9f %%}\n\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);

            /* Print matrix */
            if (N <= 20){
                cout << "Matrix A:" << endl;
                for (int r = 0; r < N; ++r) {
                    for (int c = 0; c < N; ++c) {
                        cout << setw(3) << *(A_glob + N*c + r) << " ";
                    }
                    cout << "\n";
                }
                cout << endl;
            }
        }
 
        
    }
    

    tStart = clock();
    Stopwatch<> decomposing_sw;
 
    /*****************************************
     * HERE BEGINS THE MOST INTERESTING PART *
     *****************************************/

    // A_loc = new double[nrows*ncols]; // the local matrix
    // for (int i = 0; i < nrows*ncols; ++i) *(A_loc+i)=0.;
 
    /* Scatter matrix */
    int sendr = 0, sendc = 0, recvr = 0, recvc = 0;
    int nr, nc = Nb;
    for (int r = 0; r < N; r += Nb, sendr=(sendr+1)%procrows) {
        sendc = 0;
        // Number of rows to be sent
        // Is this the last row block?
        nr = Nb;
        if (N-r < Nb)
            nr = N-r;
 
        for (int c = 0; c < N; c += Nb, sendc=(sendc+1)%proccols) {
            // NuNber of cols to be sent
            // Is this the last col block?
            nc = Nb;
            if (N-c < Nb)
                nc = N-c;
 
            if (mpiroot) {
                // cout << "Sending block (" << r << "," << c << "," << nr << "," << nc << ")" << endl;
                if (!can_alloc){
                    double* tmp_ptr = new double[nr*nc];
                    srand(time(0));
                    for (int ti = 0; ti < nr * nc; ++ti)
                    {
                        tmp_ptr[ti] = (double)rand() / RAND_MAX;
                    }
                    // if this block contains diagonal elements
                    if ((c <= r) && (c + nc >= r + nr)){
                        for (int tj = c; tj < c + nc; ++tj){
                            tmp_ptr[(tj-c) * nc + (tj-c)] += N;
                        }
                    }
                    // Send a nr-by-nc submatrix to process (sendr, sendc)
                    Cdgesd2d(ctxt, nr, nc, tmp_ptr, nc, sendr, sendc);
                    delete[] tmp_ptr;
                } else{
                    // Send a nr-by-nc submatrix to process (sendr, sendc)
                    Cdgesd2d(ctxt, nr, nc, A_glob + N*c + r, N, sendr, sendc);
                }

                
            }
 
            if (myrow == sendr && mycol == sendc) {
                A_loc = new double[nr * nc]; // the local matrix
                lda=min(nr, Nb);
                // Receive the same data
                // The leading dimension of the local matrix is nrows!
                Cdgerv2d(ctxt, nr, nc, A_loc + nrows*recvc + recvr, nrows, 0, 0);
                recvc = (recvc+nc) % ncols;
            }
 
        }
 
        if (myrow == sendr)
            recvr = (recvr+nr)%nrows;
    }
 
    
    /* Print local matrices */
    if (N <= 20) {
        for (int id = 0; id < numproc; ++id) {
            if (id == myid) {
                cout << "A_loc on node " << myid << endl;
                for (int r = 0; r < nr; ++r) {
                    for (int c = 0; c < nc; ++c)
                        cout << setw(3) << *(A_loc+nrows*c+r) << " ";
                    cout << endl;
                }
                cout << endl;
            }
            Cblacs_barrier(ctxt, "All");
        }
    }

    /* DescInit */
    int info=0;
    int descA[9];
    
    descinit_(descA, &N, &N, &Nb, &Nb, &iZERO, &iZERO, &ctxt, &lda, &info);

    if(mpiroot){
        if(verbose == 1){
            if (info == 0){
                cout << "Description init sucesss!" << endl;
            }
            else if (info < 0){
            cout << "Error Info < 0: if INFO = -i, the i-th argument had an illegal value" << endl
                << "Info = " << info << endl;
            }
        }
        // Cblacs_barrier(ctxt, "All");
    }

    //psgesv_(n, 1, al, 1,1,idescal, ipiv, b, 1,1,idescb,  info) */
    //    psgesv_(&n, &one, al, &one,&one,idescal, ipiv, b, &one,&one,idescb,  &info);
    //pXelset http://www.netlib.org/scalapack/tools/pdelset.f


    /* CHOLESKY HERE */
    info = 0;
    MPIt1=MPI_Wtime();

    int IA = 1;
    int JA = 1;
    pdpotrf_("L", &ord, A_loc, &IA, &JA, descA, &info);

    for (int id = 0; id < numproc; ++id) {
        Cblacs_barrier(ctxt, "All");
    }

    MPIt2 = MPI_Wtime();

    MPIelapsed=MPIt2-MPIt1;

    if(mpiroot){
        // std::cout<<"Cholesky MPI Run Time" << MPIelapsed<<std::endl;
        printf("{%% decomposing_matrix_mpi_counter %%}{%% %.9f %%}\n\n", MPIelapsed);

        if(info == 0){
            std::cout<< "SUCCESS" << std::endl;
        }
        if(info < 0){
            cout << "info < 0:  If the i-th argument is an array and the j-entry had an illegal value, then INFO = -(i*100+j), if the i-th argument is a scalar and had an illegal value, then INFO = -i. " << endl;
            cout << "info = " << info << endl;
        }
        if(info > 0){
            std::cout << "matrix is not positve definte" << std::endl;
        }
    }

    //sanity check set global matrix to zero before it's recieved by nodes
    if(mpiroot){
        A_glob2 = A_glob;
        // if we want to store the result in a second matrix, uncomment the below lines
        // A_glob2 = allocate_memory(N);
        // for (int r = 0; r < N; ++r) {
        //     for (int c = 0; c < N; ++c) {
        //         A_glob2[c *N + r]  = 0; 
        //     }
        // }
    }
 
    /* Gather matrix */
    if (A_glob2 != NULL){
        sendr = 0;
        for (int r = 0; r < N; r += Nb, sendr=(sendr + 1) % procrows) {
            sendc = 0;
            // NuNber of rows to be sent
            // Is this the last row block?
            int nr = Nb;
            if (N-r < Nb)
                nr = N-r;
    
            for (int c = 0; c < N; c += Nb, sendc=(sendc+1)%proccols) {
                // NuNber of cols to be sent
                // Is this the last col block?
                int nc = Nb;
                if (N-c < Nb)
                    nc = N-c;
    
                if (myrow == sendr && mycol == sendc && r >= c) {
                    // Send a nr-by-nc submatrix to process (sendr, sendc)
                    Cdgesd2d(ctxt, nr, nc, A_loc + nrows*recvc + recvr, nrows, 0, 0);
                    recvc = (recvc + nc) % ncols;
                }
    
                if (mpiroot && r >= c) {
                    // Receive the same data
                    // The leading dimension of the local matrix is nrows!
                    Cdgerv2d(ctxt, nr, nc, A_glob2 + N*c + r, N, sendr, sendc);
                }
    
            }
    
            if (myrow == sendr)
                recvr = (recvr+nr)%nrows;
        }
    }

    decomposing_sw.stop();
    if (mpiroot){
        printf("{%% decomposing_matrix %%}{%% %.9f %%}\n\n", decomposing_sw.elapsed()/1e6);
        printf("{%% total_process_time %%}{%% %.9f %%}\n\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);
    }
 
    /* Print test matrix */
    if (mpiroot && N <= 20 && A_glob2 != NULL) {
        cout << "Matrix L:" << endl;
        for (int r = 0; r < N; ++r) {
            for (int c = 0; c <= r; ++c) {
                cout << setw(3) << *(A_glob2+N*c+r) << " ";
            }
            cout << endl;
        }
    }
 
    /************************************
     * END OF THE MOST INTERESTING PART *
     ************************************/
 
    /* Release resources */
    delete[] A_glob;
    //delete[] A_glob2;
    delete[] A_loc;
    Cblacs_gridexit(ctxt);
    MPI_Finalize();
}