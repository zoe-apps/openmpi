#!/bin/bash

/sbin/ifconfig -a
my_ip=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1' | head -n 1)
service ssh start
service ssh status

echo "MY IP: $my_ip"
echo "MPI uses the hostfile at: $MPI_HOSTS_FILE"

ompi_info -a | grep btl

[ -z $NUM_SLOTS ] && NUM_SLOTS=1
[ $NUM_SLOTS -le 0 ] && NUM_SLOTS=1

my_slots=$NUM_SLOTS
# my_slots=1 # only one process per machine

MPI_HOME=/home/mpirun
[ -z "$WORKSPACE" ] && WORKSPACE=$MPI_HOME
SHARED_DIR=$WORKSPACE/cloud
[ -z  "$MPI_USER" ] && MPI_USER=mpirun
mkdir -p $SHARED_DIR && chown ${MPI_USER}:${MPI_USER} $SHARED_DIR

BUILD_COMMAND="mpic++ $MPI_HOME/test_cholesky.cpp -o $SHARED_DIR/cpp_mpi_scalapack  -lscalapack-openmpi \
            -lblacsCinit-openmpi  -lblacs-openmpi  -llapack -lopenblas -lf77blas -lcblas -lpthread  -latlas -lgfortran -std=c++11"

# we need to put the executable file in ALL machines !!!
# one solution is putting them into folder /home/mpirun/cloud
# however, container doesn't allow us to mount directory using NFS

if [ "$TYPE" == "master" ]; then

    echo "/home/mpirun/cloud *(rw,sync,no_root_squash,no_subtree_check)" >> /etc/exports
    exportfs -a  && service nfs-kernel-server restart

    eval "$BUILD_COMMAND"
    chown $MPI_USER:$MPI_USER $SHARED_DIR/cpp_mpi_scalapack

    num_entries=$(cat $MPI_HOSTS_FILE | wc -l)
    num_tries=0
    until [ $num_entries -eq $NUM_WORKERS ] || [ $num_tries -eq 40 ];
    do
        num_entries=$(cat $MPI_HOSTS_FILE | wc -l)
        echo -ne "Waiting for $(awk "BEGIN { print $NUM_WORKERS - $num_entries}"  ) workers...\033[0K\r"
        num_tries=$(( $num_tries + 1 ))
        sleep 5s
    done

    echo "Run the cluster with $num_entries out of $NUM_WORKERS machines"
    
    echo "localhost slots=$my_slots max-slots=$my_slots" >> $MPI_HOSTS_FILE
    echo "Content of the hostfile:"
    cat $MPI_HOSTS_FILE
    
    cd ${MPI_HOME}

    COMMAND="export SHARED_DIR=$SHARED_DIR;export OPENBLAS_NUM_THREADS=$my_slots;export USE_THREAD=1;$@"

    if [ "$COMMAND" != "/bin/bash" ]; then
        su -p mpirun << EOF
        [ -z "$PROCESSES_PER_ROW" ] && PROCESSES_PER_ROW=1

        echo "User's command: $COMMAND"
        
        eval "$COMMAND"
EOF

    # cut -d' ' -f1 $MPI_HOSTS_FILE | xargs -I{} echo {}
    # cut -d' ' -f1 $MPI_HOSTS_FILE | xargs -I{} ssh {} 'shutdown -h 0'

    fi

else
    echo "$MASTER_IP master" >> /etc/hosts

    sleep 5s

    mount -t nfs ${MASTER_IP}:$HOME/cloud ~/cloud
    can_mount=0
    retries=0
    status=1
    while [ "$status" -ne 0 ] && [ "$retries" -lt 3 ]; do
        mount -t nfs ${MASTER_IP}:$HOME/cloud ~/cloud
        status=$?
        [[ $status -eq 0 ]] && can_mount=1 && echo "Mount ~/cloud to ${MASTER_IP}:$HOME/cloud OK"
        retries=$(( $retries + 1 ))
        sleep ${retries}s
    done

    echo "Can mount NFS:`[ "$can_mount" -eq 1 ] && echo Yes || echo No`"
    [ ! $can_mount -eq 1 ] && eval "$BUILD_COMMAND" && chown $MPI_USER:$MPI_USER $SHARED_DIR/cpp_mpi_scalapack
    
    [ "$SSH_PORT" == "" ] && SSH_PORT=22

    retries=0
    status=1
    while [ "$status" -ne 0 ] && [ "$retries" -lt 3 ]; do
        ssh -i /home/mpirun/.ssh/id_rsa -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" \
        -p $SSH_PORT mpirun@$MASTER_IP "echo $my_ip slots=$my_slots max-slots=$my_slots >> $MPI_HOSTS_FILE"
        status=$?
        [[ $status -eq 0 ]] && echo "Register me to the master successfullly"
        retries=$(( $retries + 1 ))
        sleep ${retries}s
    done
    
    
    echo "Worker initialized!"
    tail -f /dev/null
fi




