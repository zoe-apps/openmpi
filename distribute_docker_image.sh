#!/bin/sh

# This script is run by GitLab CI in a container

# cat /scripts/distribute_docker_image.sh
# echo "---------------------------------"

# # remove all lines from "docker save....". We will use our script instead !
sed -n '/docker save/q;p' /scripts/distribute_docker_image.sh > /tmp/script1.sh
chmod u+x /tmp/script1.sh
. /tmp/script1.sh
# cat script1.sh
rm -rf /tmp/script1.sh

# HOSTS="bf5 bf7 bf8 bf9 bf10 bf12 bf13 bf14 bf15 bf16 bf17 bf18 bf19 bf20 bf21 bf22 deepfoot1"

# split HOSTS into an array
# the below commented commands doesn't work in /bin/sh (default in gitlab.eurecom.fr)
# hosts=(`echo $HOSTS | sed 's/\s+/\n/g'`)
# num_hosts=${#hosts[@]}

num_hosts=$(echo $HOSTS | wc -w)
HOSTS=`echo $HOSTS | sed 's/\s+/ /g'`

echo "Num hosts: $num_hosts"
echo $HOSTS | sed -E -e 's/[[:blank:]]+/\n/g'

# distribute image to hosts in 6 parallel threads
batch=5

distribute() {
    residual=$1
    local i
    local h
    for i in `seq 0 $(( $num_hosts - 1 ))`; do
        # the below commented commands doesn't work in /bin/sh (default in gitlab.eurecom.fr)
        # [[ $(( i % $batch )) -eq $residual ]] \
        #     && echo "Sending image to host ${hosts[$i]}..." \
        #     && cat img.tar | docker ${DOCKER_OPTS} -H ${hosts[$idx]}.containers.bigfoot.eurecom.fr:${DOCKER_PORT} load

        if [[ $(( i % $batch )) -eq $residual ]]; then
            h=$(echo $HOSTS | cut -d' ' -f$(( $i + 1 )))
            echo "Sending image to host $h..."
            docker ${DOCKER_OPTS} -H $h.containers.bigfoot.eurecom.fr:${DOCKER_PORT} load -i img.tar
        fi
    done
}

docker save $1 > img.tar

for bi in `seq 0 $batch`; do
    distribute $bi &
done

wait
rm -f img.tar